import Vue from "vue";
import VueRouter from "vue-router";
import NewInternationalExecutive from "../views/NewInternationalExecutive.vue";
import ContactInfo from "../components/ContactInfo.vue";
import NewProject from "../views/NewProject.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/new",
    name: "NewInternationalExecutive",
    component: NewInternationalExecutive
  },
  {
    path: "/contact-info",
    name: "ContactInfo",
    component: ContactInfo
  },
  {
    path: "/new-project",
    name: "NewProject",
    component: NewProject
  }
];

const router = new VueRouter({
  mode: "history",
  routes
});

export default router;
